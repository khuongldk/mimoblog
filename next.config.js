const path = require("path");

module.exports = {
  async rewrites() {
    return [
      {
        source: "/",
        destination: "/",
      },
      {
        source: "/",
        destination: "/",
      },
    ];
  },
  sassOptions: {
    includePaths: [path.join(__dirname, "styles")],
  },
};
